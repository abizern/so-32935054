//
//  DetailViewController.swift
//  SO-32935054
//
//  Created by Abizer Nasir on 04/10/2015.
//  Copyright © 2015 Jungle Candy Software. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet var label: UILabel!

    var displayText: String = ""


    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        label.text = displayText
    }

}
