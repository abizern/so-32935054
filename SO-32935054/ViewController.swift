//
//  ViewController.swift
//  SO-32935054
//
//  Created by Abizer Nasir on 04/10/2015.
//  Copyright © 2015 Jungle Candy Software. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    enum Selection: String {
        case NoSelection = ""
        case RedButton = "Has elegido un regalo de cumpleaños"
        case BlueButton = "Has elegido un regalo de aniversario"
        case WhiteButton = "Has elegido un regalo casual"
    }

    @IBOutlet var colorLabel: UILabel!
    @IBOutlet var nextButton: UIButton!

    var selection: Selection = .NoSelection {
        didSet {
            colorLabel.text = selection.rawValue
            if selection == .NoSelection {
                nextButton.hidden = true
            } else {
                nextButton.hidden = false
            }
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        colorLabel.text = selection.rawValue
        nextButton.hidden = true
    }

    @IBAction func onRed(sender: UIButton) {
        selection = .RedButton
    }

    @IBAction func onBlue(sender: UIButton) {
        selection = .BlueButton
    }

    @IBAction func onWhite(sender: UIButton) {
        selection = .WhiteButton
    }


    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        guard
            segue.identifier == "ShowDetail",
            let destinationVC = segue.destinationViewController as? DetailViewController
            else { return }

        destinationVC.displayText = selection.rawValue
    }


}

